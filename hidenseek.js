"use strict";
const fs = require('fs');
const pp = require('./poke-promise');
const random = require('./random');
const numFolders = require('./numfolders');
// const pokes = require('./pokemons');

exports.hide = function(folderName, PokemonList) {
	var fileHide = './' + folderName + '/0' + random(1,9) + '/pokemon.txt';
	return new Promise((resolve, reject) => {
		pp
			.makedir(folderName)
			.then(() => pp.makefolders(folderName))
			// .then(() => fs.readdir('./test', (err, items) => console.log(items)))
			.then(() => pp.createfile(fileHide,''))
			.then(() => pp.createotherfiles(folderName, fileHide, ''))
			.then(() => pp.writefile(fileHide, PokemonList))
			.then(() => console.log('Покемон(ы) спрятан(ы)'))
			.then(() => pp.readfile(fileHide))
			.then((content) => resolve(content))
			.catch(err => console.log(err));
	});	
}

exports.seek = function(folderName, fileHide) {
	return new Promise((resolve, reject) => {
		pp
			.readdir(folderName)
			.then((folderName) => pp.pokemonlist(folderName))
			.then((pokemons) => resolve(pokemons))
			.catch(err => console.log(err));
	});

}
