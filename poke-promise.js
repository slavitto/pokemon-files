"use strict";
const fs = require('fs');
const random = require('./random');
const numFolders = require('./numfolders');

exports.makedir = function (folderName) {
	return new Promise((resolve, reject) => {
		fs.mkdir(folderName, (err) => {
			if (err) return reject(err);
			resolve();
		});
	});
}

exports.makefolders = function (folderName) {
	return new Promise((resolve, reject) => {
		Promise.all([numFolders.forEach(numFolder => exports.makedir(folderName + numFolder))])
			.then(result => resolve())
			.catch(err => reject(err));
	});
}

exports.createfile = function (fileName, content) {
	return new Promise((resolve, reject) => {
		fs.writeFile(fileName, content, { encoding: 'utf-8' }, (err) => {
			if (err) return reject(err);
			resolve();
		});
	});
}

exports.createotherfiles = function (folderName, fileHide, content) {
	return new Promise((resolve, reject) => {
		for(var i=1, fName; i <= 5; i++) {
			fName = './' + folderName + numFolders[random(0,9)] + '/pokemon.txt';
			if(fName !== fileHide) {
				fs.writeFile(fName, content, { encoding: 'utf-8' }, (err) => {
					if (err) return reject(err);
					resolve();
				});
			}
		}
	});
}

exports.writefile = function (fileName, PokemonList) {
	return new Promise((resolve, reject) => {
		let k, fileContent;
		PokemonList.length < 3 ? k = PokemonList.length : k = 3;
		for(let i=0; i < k; i++) {
			fileContent = PokemonList[random(0,(PokemonList.length-1))] + '\r\n';
			fs.appendFile(fileName, fileContent, (err) => {
				if (err) return reject(err);
				resolve();
			});
		}
	});
}

exports.readfile = function (fileName) {
	return new Promise((resolve, reject) => {
			fs.readFile(fileName, { encoding: 'utf-8' }, (err, content) => {
				if (err) return reject(err);
				return resolve(content);
			});
	});
}

exports.readdir = function (folderName) {
	return new Promise((resolve, reject) => {
		numFolders.forEach(numFolder => {
			fs.readdir(folderName + numFolder, (err, files) => {
			if(err) return console.error(err);
			if (files) files.forEach(file => {
				fs.stat(folderName + numFolder + '/' + file, function(err, stats) { 
					if(stats['size'] > 0) 
					return resolve(folderName + numFolder) }); 
				});
			});
		});
	});
}

exports.pokemonlist = function pokemonList(folderName) {
	return new Promise((resolve, reject) => {
		fs.readFile(folderName + '/pokemon.txt', { encoding: 'utf-8' }, (err, content) => {
				if (err) return console.error(err);
				var PokemonList = content.split('\r\n');
				PokemonList.pop();
				// console.log(PokemonList);
				return resolve(PokemonList);
		});
	});
}

exports.pokemonlevel = function pokemonLevel(PokemonList) {
	return new Promise((resolve, reject) => {
		var pokeList = PokemonList.join('|').split('|'), pokeLevel = 0;
			for (var p = 0; p < pokeList.length; p++) {
				if(p % 2) pokeLevel += parseInt(pokeList[p]);
			}
		return console.log('Сумма уровней покемонов = ' + pokeLevel);
	});
}
