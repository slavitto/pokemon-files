"use strict";
const hs = require('./hidenseek');
const pokes = require('./pokemons');

var pokemons;

hs
	.hide('test', pokes)
	.then((content) => console.log(content))
	.then(() => hs.seek('test'))
	.then((pokemons) => console.log('Покемоны найдены \n' + pokemons))
	.catch(err => console.log(err));